﻿using BrewdogDownload;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace BrewdogStockSync
{
    class Program
    {
        static void Main(string[] args)
        {
            string strToken = GetToken();

            StockSync(strToken);
        }

        public static string GetToken()
        {
            string strReturn = "";

            Uri serviceUri;
            serviceUri = new Uri("https://www.brewdog.com/rest/uk/V1/integration/admin/token");
            //   serviceUri = new Uri("https://brewdog.sonassi.devsbydevs.com/rest/uk/V1/integration/admin/token");

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(serviceUri);

            request.Method = "POST";
            string auth = B64Enc(ConfigurationManager.AppSettings["ID"] + ":" + ConfigurationManager.AppSettings["Secret"]);
            //   request.Headers.Add("Authorization: Bearer " + auth);
            string Cred = "{\"username\":\"cygnia.bot\", \"password\":\"4QKKD54mgtXJpNCs\"}";
            //    string Cred = "{\"username\":\"james.colwill\", \"password\":\"doggydogdog1\"}";
            byte[] payload = Encoding.UTF8.GetBytes(Cred);
            request.ContentLength = payload.Length;
            request.ContentType = "application/json; charset=utf-8";
            request.ContentType = "application/json";

            try
            {
                Stream payloadStream = request.GetRequestStream();
                payloadStream.Write(payload, 0, payload.Length);
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new Exception(String.Format(
                    "Server error (HTTP {0}: {1}).",
                    response.StatusCode,
                    response.StatusDescription));
                }
                else
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        strReturn = reader.ReadToEnd().Replace("\"", "");
                    }
                }
            }
            catch (WebException ex)
            {
                using (var stream = ex.Response.GetResponseStream())
                using (var reader2 = new StreamReader(stream))
                {
                    string message = reader2.ReadToEnd();
                }
            }

            return strReturn.Replace("\n", "");
        }

        public static string StockSync(string strToken)
        {
            string strSKU = "";
            string strID = "";
            int RecordCount = 0;
            string strConn = ConfigurationManager.ConnectionStrings["ils"].ConnectionString;
            try
            {
                SqlConnection objConn = new SqlConnection();
                objConn.ConnectionString = strConn;
                objConn.Open();
                SqlCommand objCommand = objConn.CreateCommand();
                objCommand.CommandText = "custom_Brewdog_Stock";
                objCommand.Connection = objConn;
                SqlDataReader dRead = objCommand.ExecuteReader();

                while (dRead.Read())
                {
                    Uri serviceUri;
                    string strItem = dRead["ITEM"].ToString();
                    int strQty = Convert.ToInt32(dRead["Available"].ToString().Replace(".00000", ""));
                    string strOOS = "0";
                    RecordCount++;

                    serviceUri = new Uri("https://www.brewdog.com/rest/uk/V1/products/" + strItem + "/stockItems/1");
                    // serviceUri = new Uri("https://brewdog.sonassi.devsbydevs.com/rest/uk/V1/products/" + strItem + "/stockItems/1");

                    string strJson = "{\"stockItem\":{\"qty\":" + strQty + "}}";

                    if (strQty != 0)
                    {
                        strOOS = "1";
                    }

                    strJson = "{\"stockItem\":{\"qty\":" + strQty + ", \"is_in_stock\":" + strOOS + "}}";

                    byte[] payload = Encoding.UTF8.GetBytes(strJson);

                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(serviceUri);
                    request.Method = "PUT";
                    request.ContentType = "application/json";
                    request.Headers.Add("Authorization: Bearer " + strToken);
                    request.ContentLength = payload.Length;
                    try
                    {
                        Stream payloadStream = request.GetRequestStream();
                        payloadStream.Write(payload, 0, payload.Length);
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            throw new Exception(String.Format(
                            "Server error (HTTP {0}: {1}).",
                            response.StatusCode,
                            response.StatusDescription));
                        }
                        else
                        {
                            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                            {
                                var result = reader.ReadToEnd().Replace("\"", "");
                            }
                        }

                    }
                    catch (WebException wex)
                    {
                        var fullresp = wex.Message;
                        string strresp = "";

                        if (fullresp != null)
                        {
                            File.AppendAllText(@"c:\temp\JTE-FullLog.txt", "Item Sync Call - " + DateTime.Now + Environment.NewLine + fullresp + Environment.NewLine + strItem);

                            ProcHist("API Failures", "Failure", fullresp + " - " + strItem, "Stock Sync Post", "Brewdog API", strItem, "", "", "Failure", "", "", "BrewdogAPI");
                        }

                        DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(List<JustEatDispatchError>));
                        object objResponse = jsonSerializer.ReadObject(wex.Response.GetResponseStream());
                        List<JustEatDispatchError> jsonResponse = objResponse as List<JustEatDispatchError>;
                        List<BrewdogErrors.RootObject> l = new List<BrewdogErrors.RootObject>();
                        foreach (JustEatDispatchError n in jsonResponse)
                        {
                            BrewdogError neh = new BrewdogError();
                            neh.id = n.id;
                            neh.status = n.status;
                            List<string> errors = new List<string>();
                            foreach (Error e in n.errors)
                            {
                                errors.Add(e.code + " - " + e.title);
                            }
                            neh.errors = errors;
                            //  l.Add(neh);
                        }
                        //    return "";
                    }
                    //  return "";

                }


                objConn.Close();
                dRead.Dispose();

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("globe@cygnia.net");
                msg.To.Add("neil.sumner@brewdog.com");
                msg.To.Add("james.colwill@cygnia.net");
                msg.To.Add("naome.kinsey@brewdog.com");
                msg.To.Add("scott.merrick@cygnia.net");
                msg.Subject = "Brewdog Stock Sync Successfull : " + RecordCount.ToString() + " Records: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                msg.IsBodyHtml = true;
                msg.Body = "<br /> <br /> " + "Brewdog Stock Sync Successfull : " + RecordCount.ToString() + " Records" + ". <br /> <br />";
                SmtpClient smtp = new SmtpClient();
                smtp.Send(msg); //send email
                msg.Dispose();

                string UpdateDate = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss");

                string strConn1 = ConfigurationManager.ConnectionStrings["globe"].ConnectionString;

                SqlConnection objConn1 = new SqlConnection();
                objConn1.ConnectionString = strConn1;
                objConn1.Open();

                SqlCommand objCommand1 = objConn1.CreateCommand();

                objCommand1.CommandText = "update generic_config_detail set identifier1 = '" + UpdateDate + "' where record_type = 'LAST_TIME_STAMP' and value = 'Brewdog-StockSync'";
                objCommand1.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                File.AppendAllText(@"c:\temp\JTE-FullLog.txt", "Stock Sync Fail - " + DateTime.Now + Environment.NewLine + ex.Message + Environment.NewLine);


                MailMessage msg = new MailMessage();
                msg.From = new MailAddress("globe@cygnia.net");
                msg.To.Add("ithelpdesk@cygnia.net");
                msg.Subject = "Brewdog Stock Sync Failure : Please Rerun on DC14 " + DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                msg.IsBodyHtml = true;
                msg.Body = "Error :<br /> <br /> " + "Brewdog Stock Sync Failure : Please Rerun on DC14" + ". <br /> <br /> " + ex.Message;
                SmtpClient smtp = new SmtpClient();
                smtp.Send(msg); //send email
                msg.Dispose();
            }

            return "1";
        }

        public static string B64Enc(string sData)
        {

            byte[] encodingDataASBytes = System.Text.Encoding.UTF8.GetBytes(sData);

            string sReturnValues = System.Convert.ToBase64String(encodingDataASBytes);

            return sReturnValues;
        }

        public static void ProcHist(string ProcType, string ProcAction, string ProcText, string Ident1, string Ident2, string Ident3, string Ident4, string Ident5, string Ident6, string Ident7, string Ident8, string Process)
        {

            string strConnString = ConfigurationManager.ConnectionStrings["globe"].ConnectionString;
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(strConnString);
            SqlCommand objCommand = new SqlCommand();
            objCommand.Connection = conn;

            objCommand.CommandText = "sys_PROCESS_HISTORY_v2";
            objCommand.CommandType = System.Data.CommandType.StoredProcedure;

            conn.Open();

            try
            {
                objCommand.Parameters.Clear();
                objCommand.Parameters.Add("@PROCESS_TYPE", SqlDbType.NVarChar, 25).Value = ProcType;
                objCommand.Parameters.Add("@PROCESS_ACTION", SqlDbType.NVarChar, 25).Value = ProcAction;
                objCommand.Parameters.Add("@PROCESS_TEXT", SqlDbType.NVarChar, 4000).Value = ProcText;
                objCommand.Parameters.Add("@IDENTIFIER1", SqlDbType.NVarChar, 25).Value = Ident1;
                objCommand.Parameters.Add("@IDENTIFIER2", SqlDbType.NVarChar, 25).Value = Ident2;
                objCommand.Parameters.Add("@IDENTIFIER3", SqlDbType.NVarChar, 25).Value = Ident3;
                objCommand.Parameters.Add("@IDENTIFIER4", SqlDbType.NVarChar, 25).Value = Ident4;
                objCommand.Parameters.Add("@IDENTIFIER5", SqlDbType.NVarChar, 25).Value = Ident5;
                objCommand.Parameters.Add("@IDENTIFIER6", SqlDbType.NVarChar, 25).Value = Ident6;
                objCommand.Parameters.Add("@IDENTIFIER7", SqlDbType.NVarChar, 25).Value = Ident7;
                objCommand.Parameters.Add("@IDENTIFIER8", SqlDbType.NVarChar, 25).Value = Ident8;
                objCommand.Parameters.Add("@PROCESS_STAMP", SqlDbType.NVarChar, 100).Value = Process;
                objCommand.Parameters.Add("@USER_STAMP", SqlDbType.NVarChar, 50).Value = "Import";
                objCommand.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                //  CreateEventLog("Dalepak.Integration.OL", "Application", "ProcessHistoryError" + "\n" + error.InnerException);
                conn.Close();
            }
            conn.Close();
            conn = null;
        }

    }
}
