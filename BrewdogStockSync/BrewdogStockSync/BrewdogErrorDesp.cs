﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrewdogDownload
{
    public class BrewdogError
    {
        public int id { get; set; }
        public int status { get; set; }
        public List<string> errors { get; set; }
    }

    public class Error
    {
        public string code { get; set; }
        public string title { get; set; }
    }

    public class JustEatAcceptError
    {
        public int status { get; set; }
        public List<Error> errors { get; set; }
        public int id { get; set; }
    }

    public class JustEatDispatchError
    {
        public int status { get; set; }
        public List<Error> errors { get; set; }
        public int id { get; set; }
    }
}
